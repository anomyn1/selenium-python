# Quick reference
- **Maintained by:**
 Matthieu CALASIN
 - DockerHub : https://hub.docker.com/r/anomyn/selenium-python

# Presentation
This image provides you an execution environment for the selenium framework of the python language.  
It includes the chromedriver and the chromium engine.  
Link to the docs of Selenium to Python : https://selenium-python.readthedocs.io/

# Before run
Make sure your python script contains the options to run in headless mode.  
Example :
```python
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By

options = Options()
options.add_argument("--headless") # Runs Chrome in headless mode.
options.add_argument('--no-sandbox') # Bypass OS security model
options.add_argument('start-maximized')
options.add_argument('disable-infobars')
options.add_argument("--disable-extensions")

driver = webdriver.Chrome(options=options, executable_path='chromedriver')
 ```

# Run
```bash
docker run -tid -v <path containing your scripts>:/home/selenium --name <name of container> anomyn/selenium-python:<tag>
docker exec <name of container> python <path of your script>
```
