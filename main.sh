#!/bin/bash

#Chromium/Chromedriver version

url_last_version_chromedriver="https://chromedriver.storage.googleapis.com/LATEST_RELEASE"

# Afficher la dernière version du chromedriver/chromium
last_version_chrome=$(curl -s $url_last_version_chromedriver)

dockerfile_version_chrome=$(grep chromium Dockerfile | cut -d "=" -f 2 | cut -d "-" -f 1 | head -1)

if [[ "$last_version_chrome" != "$dockerfile_version_chrome" ]] ; then
    echo "Une version plus récente du chromedriver est disponible :" $last_version_chrome
    # Mise à jour des versions dans le dockerfile
    sed -i 's/'$dockerfile_version_chrome'/'$last_version_chrome'/g' Dockerfile

    git checkout -b "CI-chromedriver-"$last_version_chrome
    git add .
    git commit -m "CI: New version of chromedriver "$last_version_chrome
    git push origin "CI-chromedriver-"$last_version_chrome

    # Déclenchement de la pipeline Gitlab CI
    curl -X POST \
    --fail \
    -F token=$token_gitlab \
    -F "ref=CI-chromedriver-"$last_version_chrome \
    -F "variables[tag]=chromedriver-"$last_version_chrome \
    https://gitlab.com/api/v4/projects/37334790/trigger/pipeline

    # TODO : ajouter if pour vérif bon déclenchement pipeline

else
    echo "Il n'y a pas de nouvelle version de chromium et du chromedriver"
fi

# Python Version
url_last_version_python="https://www.python.org/downloads/"

last_version_python=$(curl -s https://www.python.org/downloads/ | grep "release-number" | cut -d ">" -f 3 | cut -d "<" -f 1 | cut -d " " -f 2 | head -2 | tail -n 1)
#url_search_version_python_image="https://hub.docker.com/_/python?tab=tags&page=1&name="$last_version_python"-slim-bullseye"

dockerfile_version_python=$(grep python Dockerfile | cut -d ":" -f 2 | cut -d "-" -f 1)

if [[ "$last_version_python" != "$dockerfile_version_python" ]] ; then
    echo "Une version plus récente de Python est disponible :" $last_version_python
    sed -i 's/'$dockerfile_version_python'/'$last_version_python'/g' Dockerfile

    git checkout -b "CI-python-"$last_version_python
    git add .
    git commit -m "CI: New version of python "$last_version_python
    git push origin "CI-python-"$last_version_python

    curl -X POST \
     --fail \
     -F token=$token_gitlab \
     -F "ref=CI-python-"$last_version_python  \
     -F "variables[tag]=python-"$last_version_python \
     https://gitlab.com/api/v4/projects/37334790/trigger/pipeline

else
    echo "Il n'y a pas de nouvelle version de python"
fi

# Selenium Version
url_last_version_selenium="https://pypi.org/project/selenium/#history"

last_version_selenium=$(curl -s $url_last_version_selenium | grep "card release__card" | head -1 | cut -d "/" -f 4)

dockerfile_version_selenium=$(grep "pip" Dockerfile | cut -d "=" -f 3)

if [[ "$last_version_selenium" != "$dockerfile_version_selenium" ]] ; then
    echo "Une version plus récente de selenium est disponible :" $last_version_selenium
    sed -i 's/'$dockerfile_version_selenium'/'$last_version_selenium'/g' Dockerfile

    git checkout -b "CI-selenium-"$last_version_selenium
    git add .
    git commit -m "CI: New version of selenium "$last_version_selenium
    git push origin "CI-selenium-"$last_version_selenium

    curl -X POST \
     --fail \
     -F token=$token_gitlab \
     -F "ref=CI-selenium-"$last_version_selenium \
     -F "variables[tag]=selenium-"$last_version_selenium \
     https://gitlab.com/api/v4/projects/37334790/trigger/pipeline

else
    echo "Il n'y a pas de nouvelle version de selenium"
fi