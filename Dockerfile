FROM python:3.10.5-slim-bullseye

SHELL ["/bin/bash", "-c"]

RUN apt update && apt install -y --no-install-recommends \
  chromium=103.0.5060.53-1~deb11u1 \
  chromium-driver=103.0.5060.53-1~deb11u1 \
  chromium-common=103.0.5060.53-1~deb11u1 \
  && rm -rf /var/lib/apt/lists/*

RUN pip install --no-cache-dir selenium==4.3.0

RUN useradd -d /home/selenium -m -s /bin/bash selenium

USER selenium

WORKDIR /home/selenium

EXPOSE 9515