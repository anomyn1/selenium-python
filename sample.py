from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By

options = Options()
options.add_argument("--headless") # Runs Chrome in headless mode.
options.add_argument('--no-sandbox') # Bypass OS security model
options.add_argument('start-maximized')
options.add_argument('disable-infobars')
options.add_argument("--disable-extensions")

driver = webdriver.Chrome(options=options, executable_path='chromedriver')
driver.get("file://///home/selenium/example.html")

def navigate():
    name = driver.find_element(By.XPATH, "/html/body/form/fieldset/input[1]")
    name.send_keys("test")

    email = driver.find_element(By.XPATH, "/html/body/form/fieldset/input[2]")
    email.send_keys("test@test.com")

    age_checkbox = driver.find_element(By.XPATH, "/html/body/form/fieldset/input[4]")
    age_checkbox.click()
    
    list = driver.find_element(By.XPATH, "/html/body/form/filedset/select/optgroup[2]/option[1]")
    list.click()

    signup = driver.find_element(By.XPATH, "/html/body/form/h1").text
    your_basic_info = driver.find_element(By.XPATH, "/html/body/form/fieldset/legend").text
    design = driver.find_element(By.XPATH, "/html/body/form/filedset/label[4]").text

    if signup == "Sign Up" and your_basic_info == "Your basic info." and design == "Design" :
        print("Le test fonctionne !")
        driver.quit()
        exit(0)
    else:
        print("Le test n'a pas fonctionné !")
        driver.quit()
        exit(1)
navigate()